const Branch = (name, leftLeaf, rightLeaf) => ({
    type: "Tree",
    name: name,
    left: leftLeaf,
    right: rightLeaf,

    //property needed to track down the finish for map
    isLeaf: false,

    toString(){
        return  "(\""+ this.name + "\"," + this.left + "," + this.right + ")";
    },
    //Not used method
    getType(){
        return this.type;
    },
    
    //recursive
    map(x){
        const Map = [];

        if(this.left.isLeaf === false){
            this.left.map(x);   
        }
        if(this.right.isLeaf === false){
            this.right.map(x);   
        }
        if(this.isLeaf === false){
            const aux = this
            aux.name = x(aux.name);
            Map.push(aux); 
        }
        return Map;
    }
});
const Leaf = () => ({
    type: "Tree",
    isLeaf: true,
    toString() {
        return "🍂";
    },
    getType() {
        return this.type;
    }
});
module.exports = {Branch, Leaf}

//const { Branch, Leaf } = require("@gerardo.munguia/tree-prototype");

const leftBranch =  Branch("foo", Leaf(), Leaf());
const rightBranch = Branch("bar", Leaf(), Leaf());
const tree = Branch("baz", leftBranch, rightBranch);

console.log(tree.left.toString()); // -> ("foo", 🍂, 🍂)
console.log(tree.map(word => `${word}!`).toString()); // -> ("baz", ("foo!", 🍂, 🍂), ("bar!", 🍂, 🍂))
console.log(tree.getType()); // -> Tree
console.log(Leaf().getType()); // -> Tree