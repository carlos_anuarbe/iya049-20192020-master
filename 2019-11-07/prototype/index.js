function Branch(name, leftLeaf, rightLeaf) {
    this.type = "Tree";
    this.name = name;
    this.left = leftLeaf;
    this.right = rightLeaf;

    //property needed to track down the finish for map
    this.isLeaf = false;
}
Branch.prototype.toString = function toString(){
    return  "(\""+ this.name + "\", " + this.left + ", " + this.right + ")";
}
//Not used method
Branch.prototype.getType = function getType(){
    return this.type;
}

//recursive
Branch.prototype.map = function map(x){
    const Map = [];
    
    if(this.left.isLeaf === false){
        this.left.map(x);
    }
    if(this.right.isLeaf === false){
        this.right.map(x);
    }
    if(this.isLeaf === false){
        const aux = this
        aux.name = x(aux.name);
        Map.push(aux);  
    }
    
    return Map.toString();
}
function Leaf(){
    this.type = "Tree";
    this.isLeaf = true;
}

Leaf.prototype.toString = function toString(){
    return "🍂";
};

Leaf.prototype.getType = function getType(){
    return this.type;
};
module.exports = {Branch, Leaf}

//const { Branch, Leaf } = require("@carlos.anuarbe/tree-prototype");

const leftBranch = new Branch("foo", new Leaf(), new Leaf());
const rightBranch = new Branch("bar", new Leaf(), new Leaf());
const tree = new Branch("baz", leftBranch, rightBranch);

console.log(`${tree.left}`); // -> ("foo", 🍂, 🍂)
console.log(`${tree.map(word => `${word}!`)}`); // -> ("baz", ("foo!", 🍂, 🍂), ("bar!", 🍂, 🍂))
console.log(tree.getType()); // -> Tree
console.log(new Leaf().getType()); // -> Tree