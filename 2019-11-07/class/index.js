class Branch {
    constructor(name, leftLeaf, rightLeaf) {
        this.type = "Tree";
        this.name = name;
        this.left = leftLeaf;
        this.right = rightLeaf;

        //property needed to track down the finish for map
        this.isLeaf = false;
    }
    toString(){
        return  "(\""+ this.name + "\"," + this.left + "," + this.right + ")";
    }
    //Not used method
    getType(){
        return this.type;
    }
    
    //recursive
    map(x){
        const Map = [];

        if(this.left.isLeaf === false){
            this.left.map(x);   
        }
        if(this.right.isLeaf === false){
            this.right.map(x);   
        }
        if(this.isLeaf === false){
            const aux = this
            aux.name = x(aux.name);
            Map.push(aux); 
        }
        return Map;
    }
}
class Leaf {
    constructor() {
        this.type = "Tree";
        this.isLeaf = true;
    }
    toString() {
        return "🍂";
    }
    getType() {
        return this.type;
    }
}
module.exports = {Branch, Leaf}




//const { Branch, Leaf } = require("@gerardo.munguia/tree-prototype");

const leftBranch = new Branch("foo", new Branch("baar", new Leaf(), new Leaf()), new Leaf());
const rightBranch = new Branch("bar", new Leaf(), new Leaf());
const tree = new Branch("baz", leftBranch, rightBranch);


console.log(`${tree.map(word => `${word}!`)}`); // -> ("baz", ("foo!", 🍂, 🍂), ("bar!", 🍂, 🍂))
console.log(tree.getType()); // -> Tree
console.log(new Leaf().getType()); // -> Tree
console.log(`${tree.left}`); // -> ("foo", 🍂, 🍂)